package lab01.methods;

import lab01.model.Person;
import lab01.model.Student;
import lab01.model.Teacher;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonManage {
    private List<Person> personManage = new ArrayList<>();

    public PersonManage(){};

    public void input()
    {
        Scanner scanner = new Scanner(System.in);
        int iscontinute = 1;
        while (iscontinute !=0)
        {
            System.out.println("Options: ");
            System.out.println("1.Teacher");
            System.out.println("2.Student");
            System.out.println("3.Exit");
            System.out.print("Enter: ");
            int number = scanner.nextInt();
            scanner.nextLine();
            while (number< 1 || number > 3)
            {
                System.out.println("Please enter again: ");
                number = scanner.nextInt();
                scanner.nextLine();
            }
            switch (number)
            {
                case 1:
                    System.out.print("Enter name: ");
                    String name = scanner.nextLine();
                    System.out.print("Enter gender: ");
                    String gender = scanner.nextLine();
                    System.out.print("Enter phone number: ");
                    String phoneNumber = scanner.nextLine();
                    System.out.print("Enter email: ");
                    String email = scanner.nextLine();
                    System.out.print("Enter basic salary: ");
                    double basicSalary = scanner.nextDouble();
                    scanner.nextLine();
                    System.out.print("Enter subsidy: ");
                    double subsidy = scanner.nextDouble();
                    scanner.nextLine();
                    Person p = new Teacher(name,gender,phoneNumber,email,basicSalary,subsidy);
                    personManage.add(p);
                    break;
                case 2:
                    System.out.print("Enter name: ");
                    String sname = scanner.nextLine();
                    System.out.print("Enter gender: ");
                    String sgender = scanner.nextLine();
                    System.out.print("Enter phone number: ");
                    String sphoneNumber = scanner.nextLine();
                    System.out.print("Enter email: ");
                    String semail = scanner.nextLine();
                    System.out.print("Enter student id: ");
                    String studentId = scanner.nextLine();
                    System.out.print("Enter theory: ");
                    double theory = scanner.nextDouble();
                    scanner.nextLine();
                    System.out.print("Enter practice: ");
                    double practice = scanner.nextDouble();
                    scanner.nextLine();
                    Person sp = new Student(sname,sgender,sphoneNumber,semail,studentId,theory,practice);
                    personManage.add(sp);
                    break;
                case 3:
                    System.out.println("Exit");
                    iscontinute = 0;
                    break;
                default:
                    throw  new RuntimeException();

            }
        }
    }
    public void display()
    {
        for (int i = 0; i < personManage.size();i++)
        {
            personManage.get(i).display();
        }
    }

    public void displayTeacher()
    {
        for (int i = 0; i < personManage.size();i++)
        {
            if (personManage.get(i).getClass().equals(Teacher.class))
            {
                Teacher t = (Teacher) personManage.get(i);
                if (t.getBasicSalary() > 1000)
                {
                    t.display();
                }
            }
        }
    }

    public void report()
    {
        for (int i = 0; i < personManage.size();i++)
        {
            if (personManage.get(i).getClass().equals(Student.class))
            {
                if (personManage.get(i).getMark() >= 6)
                {
                    personManage.get(i).display();
                }
            }
        }
    }
    public void updateStudent(int id)
    {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i< personManage.size();i++)
        {
            if (personManage.get(i).getClass().equals(Student.class))
            {
                Student s = (Student) personManage.get(i);
                personManage.remove(i);
                int isContinute = 1;
                while (isContinute !=0)
                {
                    s.display();
                    System.out.println("Options: ");
                    System.out.println("1.Name: ");
                    System.out.println("2.gender");
                    System.out.println("3.Phone number");
                    System.out.println("4.email");
                    System.out.println("5.studentId");
                    System.out.println("6.theory");
                    System.out.println("7.practice");
                    System.out.println("8.Exit");
                    int number = scanner.nextInt();
                    scanner.nextLine();
                    while (number< 1 || number > 8)
                    {
                        System.out.println("Please enter again: ");
                        number = scanner.nextInt();
                        scanner.nextLine();
                    }
                    switch (number)
                    {
                        case 1:
                            System.out.print("Enter new name: ");
                            String name = scanner.nextLine();
                            s.setName(name);
                            break;
                        case 2:
                            System.out.print("Enter new gender: ");
                            String gender = scanner.nextLine();
                            s.setGender(gender);
                            break;
                        case 3:
                            System.out.print("Enter new Phone number: ");
                            String phoneNumber = scanner.nextLine();
                            s.setPhoneNumber(phoneNumber);
                            break;
                        case 4:
                            System.out.print("Enter new email: ");
                            String email = scanner.nextLine();
                            s.setEmail(email);
                            break;
                        case 5:
                            System.out.print("Enter new studentId: ");
                            String studentId = scanner.nextLine();
                            s.setStudentId(studentId);
                            break;
                        case 6:
                            System.out.print("Enter new theory: ");
                            double theory = scanner.nextDouble();
                            scanner.nextLine();
                            s.setTheory(theory);
                        case 7:
                            System.out.print("Enter new practice: ");
                            double practice = scanner.nextDouble();
                            scanner.nextLine();
                            s.setPractice(practice);
                        case 8:
                            System.out.println("Exit");
                            isContinute = 0;
                            break;
                    }
                }
                personManage.add(i,s);
            }
        }
        scanner.close();
    }

    public List getPersonManage() {
        return personManage;
    }

    public void setPersonManage(List personManage) {
        personManage = personManage;
    }
}
