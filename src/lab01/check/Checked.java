package lab01.check;

public class Checked {
    public static boolean emailCheck(String email)
    {
        String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        return email.matches(emailRegex);
    }
    public static boolean pointCheck(double point)
    {
        if (point>=0 && point<=10)
        {
            return true;
        }
        return false;
    }
}
