package lab01.model;

import lab01.model.Person;

public class Student extends Person {
    private String studentId;
    private double theory;
    private double practice;
    public Student(){};
    public Student(String name,String gender,
                   String phoneNumber,String email,
                   String studentId, double theory,
                   double practice){
        super(name,gender,phoneNumber,email);
        this.studentId = studentId;
        this.theory = theory;
        this.practice = practice;
    };

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public double getTheory() {
        return theory;
    }

    public void setTheory(double theory) {
        this.theory = theory;
    }

    public double getPractice() {
        return practice;
    }

    public void setPractice(double practice) {
        this.practice = practice;
    }

    public double calculateFinalMark()
    {
        return practice+theory;
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Student id: "+getStudentId());
        System.out.println("Theory: "+getTheory());
        System.out.println("Practice: "+getPractice());
        System.out.println();
    }

    @Override
    public double getMark() {
        super.getMark();
        return (getPractice()+getTheory())/2;
    }
}
