package lab02.courseHandle;

import lab02.model.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static lab02.courseHandle.Check.*;

public class Handle {
    public static Course input(String courseId,String courseName,double courseDuration,String courseState,String flag)
    {
        Scanner scanner = new Scanner(System.in);
        while (!codeIdvalid(courseId))
        {
            System.out.print("string of 5 characters, started by “FW” and followed by 3 digits: ");
            courseId = scanner.nextLine();
        }
        while (!statusCheck(courseState))
        {
            System.out.print("only accept 'active' or 'in-active': ");
            courseState = scanner.nextLine();
        }
        while (!flagCheck(flag))
        {
            System.out.print("only accept 'optional', 'mandatory', 'N/A': ");
            flag = scanner.nextLine();
        }
        Course c = new Course(courseId,courseName,courseDuration,courseState,flag);
        scanner.close();
        return c;
    }

    public static void displayAllCoursesWithFlag(List<Course> courseList)
    {
        for (int i = 0;i < courseList.size();i++)
        {
            if (courseList.get(i).getFlag().equals("mandatory"))
            {
                System.out.println(courseList.get(i).toString());
            }
        }
    }
    public static boolean searchById(String id,List<Course> courseList)
    {
        for (int i = 0;i < courseList.size();i++)
        {
            if (courseList.get(i).getCourseId().equals(id))
            {
                System.out.println(courseList.get(i).toString());
                return true;
            }
        }
        return false;
    }
    public static boolean searchByName(String name,List<Course> courseList)
    {
        for (int i = 0;i < courseList.size();i++)
        {
            if (courseList.get(i).getCourseName().equals(name))
            {
                System.out.println(courseList.get(i).toString());
                return true;
            }
        }
        return false;
    }
    public static boolean searchByState(String state,List<Course> courseList)
    {
        for (int i = 0;i < courseList.size();i++)
        {
            if (courseList.get(i).getCourseState().equals(state))
            {
                System.out.println(courseList.get(i).toString());
                return true;
            }
        }
        return false;
    }

    public static List setUpList()
    {
        List<Course> courseList = new ArrayList<>();
        courseList.add(new Course("FW123","Java",3,"active","mandatory"));
        courseList.add(new Course("FW153","Javascript",4,"in-active","optional"));
        return courseList;
    }
}
