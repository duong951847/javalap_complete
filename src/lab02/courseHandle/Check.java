package lab02.courseHandle;

public class Check {
    public static boolean codeIdvalid(String check)
    {
        String id = "^[FW]{2}+[0-9]{3}$";
//        String id = "^(FW|GG)+[0-9]{3}$";
        return check.matches(id);
    }
    public static boolean statusCheck(String check)
    {
        if (check.equals("active")||check.equals("in-active"))
        {
            return true;
        } else {
            return false;
        }
    }
    public static boolean flagCheck(String check)
    {
        if (check.equals("optional")||check.equals("mandatory")||check.equals("N/A"))
        {
            return true;
        }else {
            return false;
        }
    }
}
