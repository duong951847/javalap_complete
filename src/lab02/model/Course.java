package lab02.model;

public class Course {
    private String courseId;
    private String courseName;
    private double courseDuration;
    private String courseState;
    private String flag;
    public Course(){};
    public Course(String courseId,String courseName,double courseDuration,String courseState,String flag){
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseDuration = courseDuration;
        this.courseState = courseState;
        this.flag = flag;
    };

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getCourseDuration() {
        return courseDuration;
    }

    public void setCourseDuration(double courseDuration) {
        this.courseDuration = courseDuration;
    }

    public String getCourseState() {
        return courseState;
    }

    public void setCourseState(String courseState) {
        this.courseState = courseState;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "lab02.model.Course{" +
                "courseId='" + courseId + '\'' +
                ", courseName='" + courseName + '\'' +
                ", courseDuration=" + courseDuration +
                ", courseState='" + courseState + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
