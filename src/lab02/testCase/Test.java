package lab02.testCase;

import lab02.model.Course;
import org.junit.Assert;
import org.junit.Before;

import java.util.List;

import static lab02.courseHandle.Check.codeIdvalid;
import static lab02.courseHandle.Handle.*;

public class Test {
    private List<Course> input;
//    @org.junit.Test
//    public void test01()
//    {
//        Assert.assertTrue(codeIdvalid("FW123"));
//        Assert.assertTrue(codeIdvalid("FW567"));
//        Assert.assertTrue(codeIdvalid("GG567"));
////        Assert.assertTrue(codeIdvalid("0ABED"));
////        Assert.assertTrue(codeIdvalid("9EFF"));
//    }
    @Before
    public void setUp()
    {
       input = setUpList();
    }
    @org.junit.Test
    public void testId()
    {
        Assert.assertTrue(searchById("FW153",input));
    }
    @org.junit.Test
    public void testName()
    {
        Assert.assertTrue(searchByName("Javascript",input));
    }
    @org.junit.Test
    public void testState()
    {
        Assert.assertTrue(searchByState("in-active",input));
    }
}
