package lab03;

import lab03.management.MultimediaManagement;
import lab03.model.Multimedia;
import lab03.model.Song;
import lab03.model.Video;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int isContinue = 1;
        MultimediaManagement mmm = new MultimediaManagement();
        while (isContinue != 0)
        {
            System.out.println("Choose function: ");
            System.out.println("1. Add a new Video");
            System.out.println("2. Add a new Song");
            System.out.println("3. Show all multimedia");
            System.out.println("4. Exit");
            System.out.print("Your choice: ");
            int number = scanner.nextInt();
            scanner.nextLine();
            while (number< 1 || number >4)
            {
                System.out.print("Enter your choice again: ");
                number = scanner.nextInt();
                scanner.nextLine();
            }
            Multimedia m;
            switch (number)
            {
                case 1 :
                    Video v = new Video();
                    v.createVideo();
                    m = v;
                    mmm.addMultiMedia(m);
                    break;
                case 2 :
                    Song s = new Song();
                    s.createSong();
                    m = s;
                    mmm.addMultiMedia(m);
                    break;
                case 3:
                    mmm.displayMultiMedia();
                    break;
                case 4:
                    isContinue = 0;
                    System.out.print("Exit");
                    break;
            }
        }
        scanner.close();
    }
}
