package lab03.management;

import lab03.model.Multimedia;

import java.util.ArrayList;
import java.util.List;

public class MultimediaManagement {
    private List<Multimedia> listOfMultimedia = new ArrayList<>();
    public void addMultiMedia(Multimedia multimedia)
    {
        listOfMultimedia.add(multimedia);
    }
    public void displayMultiMedia() {
        for (int i = 0; i < listOfMultimedia.size(); i++) {
            listOfMultimedia.get(i).display();
        }
    }
}
