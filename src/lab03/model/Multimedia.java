package lab03.model;

import java.util.Scanner;

public abstract class Multimedia {
    private String name;
    private double duration;
    public Multimedia(){};
    public Multimedia(String name,double duration){
        this.name = name;
        this.duration = duration;
    };
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
    public void display()
    {
        System.out.println("Name: "+getName());
        System.out.println("Duration: "+getDuration());
    }

    public void createMultimedia()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        System.out.print("Enter duration: ");
        double duration = scanner.nextDouble();
        scanner.nextLine();
        setName(name);
        setDuration(duration);
    }

    @Override
    public String toString() {
        return "Multimedia{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                '}';
    }
}
