package lab03.model;

import java.util.Scanner;

public class Song extends Multimedia{
    private String singer;
    public Song(){};
    public Song(String name,double duration,String singer){
        super(name,duration);
        this.singer = singer;
    };

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public void display() {
        super.display();
        System.out.println("Singer: "+getSinger());
    }

    public void createSong() {
        super.createMultimedia();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter singer: ");
        String singer = scanner.nextLine();
        setSinger(singer);
    }

    @Override
    public String toString() {
        return "Song{" +
                "singer='" + singer + '\'' +
                '}';
    }
}
