package lab03.model;

public class Video extends Multimedia{
    public Video(){};
    public Video(String name,double duration){
        super(name,duration);
    };
    @Override
    public void display() {
        super.display();
    }
    public void createVideo()
    {
        super.createMultimedia();
    }

    @Override
    public String toString() {
        return "Video{" +
                "name='" + super.getName() + '\'' +
                ", duration=" + super.getDuration() +
                "}";
    }
}
