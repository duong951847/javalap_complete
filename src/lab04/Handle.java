package lab04;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static lab04.check.checkPhone;

public class Handle {
    public List<String> createCustomer() {
        Scanner scanner = new Scanner(System.in);
        List<String> customers = new ArrayList<>();
        while (true) {
            System.out.print("Enter name: ");
            String name = scanner.nextLine();
            System.out.print("Enter phone number: ");
            String phoneNumber = scanner.nextLine();
            while (!checkPhone(phoneNumber))
            {
                System.out.print("Enter phone number again: ");
                phoneNumber = scanner.nextLine();
            }
            System.out.print("Enter address: ");
            String address = scanner.nextLine();
            List<Order> listOfOrders = new ArrayList<>();
            int orderContinue = 1;
            while (orderContinue != 0) {
                System.out.println("Options: ");
                System.out.println("1.Add order");
                System.out.println("2.Exit");
                System.out.print("Enter: ");
                int number = scanner.nextInt();
                while (number < 1 || number > 2) {
                    System.out.print("Enter your choice again: ");
                    number = scanner.nextInt();
                    scanner.nextLine();
                }
                scanner.nextLine();
                switch (number) {
                    case 1:
                        System.out.print("Enter order name: ");
                        String orderName = scanner.nextLine();
                        System.out.print("Enter order date: ");
                        String orderDate = scanner.nextLine();
                        Order o = new Order(orderName, orderDate);
                        listOfOrders.add(o);
                        break;
                    case 2:
                        orderContinue = 0;
                        System.out.println("Exit");
                        break;
                }
            }
            Customer c = new Customer(name, phoneNumber, address, listOfOrders);
            customers.add(c.toString());
            System.out.print("Enter n or N to exit: ");
            String iscontinue = scanner.nextLine();
            if (iscontinue.equals("n")) {
                return customers;
            } else if (iscontinue.equals("N")) {
                return customers;
            }
        }
    }

    public String save(List<String> customers) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("customer.txt"));
            for (String i : customers) {
                writer.write(i);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Success";
    }

    public List<String> findAll() {
        List<String> customers = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("customer.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                customers.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public void display(List<String> l) {
        System.out.printf("Customer Name\t\t\tAddress\t\t\t Phone Number\t\t\tOrderList\n");
        for (int i = 0; i < l.size(); i++) {
            String s = l.get(i);
            int number = s.indexOf("{");
            String news = s.substring(number, s.length());
            String customerString = news;

            String customerInfoRegex = "name='(.*?)', phoneNumber='(.*?)', address='(.*?)'";
            Pattern pattern = Pattern.compile(customerInfoRegex);
            Matcher matcher = pattern.matcher(customerString);

            String name = "", phoneNumber = "", address = "";

            if (matcher.find()) {
                name = matcher.group(1);
                phoneNumber = matcher.group(2);
                address = matcher.group(3);
            }

            String orderInfoRegex = "listOfOrders=\\[(.*?)\\]";
            pattern = Pattern.compile(orderInfoRegex);
            matcher = pattern.matcher(customerString);

            String orderInfo = "";

            if (matcher.find()) {
                String order = matcher.group(1);
                orderInfo = "Order{"+order+"}";
            }
            System.out.printf("%s\t\t\t\t\t\t%s\t\t\t\t %s\t\t\t\t\t%s\n", name, address, phoneNumber, orderInfo);
        }
    }

    public List<String> search(String phone)
    {
        List<String> search = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("customer.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains(phone))
                {
                    search.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  search;
    }
    public boolean remove(String phone)
    {
        String tempFile = "temp.txt";
        File oldFile = new File("customer.txt");
        File newFile = new File(tempFile);
//        int line = 0;
        String currentline;
        try {
            FileWriter fileWriter = new FileWriter(tempFile,true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);

            FileReader fileReader = new FileReader("customer.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((currentline = bufferedReader.readLine())!= null)
            {
//                line++;
                if (!currentline.contains(phone))
                {
                    printWriter.println(currentline);
                }
            }
            printWriter.flush();
            printWriter.close();
            fileReader.close();
            bufferedReader.close();
            bufferedWriter.close();
            fileWriter.close();

            oldFile.delete();
            File dump = new File("customer.txt");
            newFile.renameTo(dump);
        }catch (IOException e)
        {
            e.printStackTrace();
        }
        return true;
    }
}