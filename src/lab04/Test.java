package lab04;


import java.util.List;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Handle h = new Handle();
        int isContinue = 1;
        while(isContinue != 0)
        {
            System.out.println("Choose function: ");
            System.out.println("1.Add new customer");
            System.out.println("2.Show All customers");
            System.out.println("3.Search customer");
            System.out.println("4.Remove customer");
            System.out.println("5.Exit");
            System.out.print("Enter: ");
            int number = scanner.nextInt();
            scanner.nextLine();
            while (number< 1 || number > 5)
            {
                System.out.print("Enter: ");
                number = scanner.nextInt();
                scanner.nextLine();
            }
            switch (number)
            {
                case 1:
                    List<String> customers = h.createCustomer();
                    System.out.println(h.save(customers));
                    break;
                case 2:
                    List<String> findAll = h.findAll();
                    h.display(findAll);
                    break;
                case 3:
                    System.out.print("Enter phone number to search: ");
                    String phone = scanner.nextLine();
                    List<String> search = h.search(phone);
                    h.display(search);
                    break;
                case 4:
                    System.out.print("Enter phone number to remove: ");
                    String phoneRemove = scanner.nextLine();
                    if (h.remove(phoneRemove))
                    {
                        System.out.println("Success");
                    }else
                    {
                        System.out.println("fail");
                    }
                    break;
                case 5:
                    isContinue = 0;
                    System.out.println("Exit");
            }
        }
        scanner.close();
    }
}
